#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include "user.h"
#include "time.h"

extern time_t time;

#if defined(__XC) || defined(HI_TECH_C)
void interrupt high_isr(void)
#elif defined (__18CXX)
#pragma code high_isr=0x08
#pragma interrupt high_isr
void high_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{
  if(INTCON & 0b00000100)   // Tmr0 overflow interrupt (4 Hz)
  {
    if (time.toggle2Hz) { // 2 Hz
      time.toggle2Hz = 0;
      time.doSet = 1;
      if (time.toggle1Hz) { // 1 Hz
        time.toggle1Hz = 0;
        time.doTick = 1;
      } else {
        time.toggle1Hz = 1;
      }
    } else {
      time.toggle2Hz = 1;
    }
    INTCON &= ~(0b00000100);
  }
}

/* Low-priority interrupt routine */
#if defined(__XC) || defined(HI_TECH_C)
void low_priority interrupt low_isr(void)
#elif defined (__18CXX)
#pragma code low_isr=0x18
#pragma interruptlow low_isr
void low_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{

}
