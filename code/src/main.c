#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"
#include "user.h"
#include "time.h"

#define U_MIN       (0)
#define U_LIMN_MAX  (160)

extern time_t time;
extern hvdc_t hvdc;
extern uint8_t btnSetReleased;

void main(void)
{

  int16_t error = 0;
  int16_t u = 0;
  int16_t integral = 0;

  HVDCinit(&hvdc, HVDC_SETPOINT_DEFAULT, HVDC_SETPOINT_FIRE);

  InitPeriph();

  HvSetAll();

  PwmDuty_10bit(0);
  //PwmDuty_10bit(115);   // 532 == 100 % (15kHz), 264 == 100 % (30kHz)

  while(1)
  {
    /* Control the step-up converter 12V to ~150V */
    error = (int16_t) (HVDCgetVoltageSetPoint(&hvdc, &time) - ADGetRes(6));

    if (u <= U_LIMN_MAX)
      integral += error;

    u = HVDC_CTRL_GAIN_P(error) + HVDC_CTRL_GAIN_I(integral);

    if (u < 0) {
      u = 0;
    } else if (u > U_LIMN_MAX) {
      u = U_LIMN_MAX;
    }

    PwmDuty_10bit((uint16_t) u);

    /* Time progresses and update nixies */
    if (time.doTick) {
      timeTick(&time);

      if (time.setTimeMode == TIMESETMODEOFF) {
        timeOutput(&time);

        if (btnSetPressed()) {
          if (time.setSecPressed++ > 2) {
            time.setSecPressed = 0;
            time.setTimeMode = TIMESETMODEMIN;
          }
        } else {
          time.setSecPressed = 0;
        }
      }
      time.doTick = 0;
    }

    /* Handle time set mode */
    if (time.setTimeMode != TIMESETMODEOFF) {
      SET_DEBUG;
      if (!btnSetPressed())
        btnSetReleased = 1;     

      if (time.doSet && btnSetReleased) {
        timeSet(&time);
        time.doSet = 0;
      }
      timeOutput(&time);
    } else {
      CLEAR_DEBUG;
    }
  }

}
