#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include "user.h"

hvdc_t hvdc = {0,0};
uint8_t btnSetReleased = 0;

uint8_t btn1Pressed(void)
{
  return (BTN1_PORT & BTN1_PIN);
}

uint8_t btn2Pressed(void)
{
  return (BTN2_PORT & BTN2_PIN);
}

uint8_t btn3Pressed(void)
{
  return (BTN3_PORT & BTN3_PIN);
}

uint8_t btnSetPressed(void)
{
  return (btn1Pressed());
}

uint8_t btnDownPressed(void)
{
  return (btn2Pressed());
}

uint8_t btnUpPressed(void)
{
  return (btn3Pressed());
}

void HvSetAll(void)
{
  uint8_t n;

  SET_HVDTA1;
  CLEAR_HVLTC1;
  for (n=0; n<HVNOUTTOT; n++) {
    CLEAR_HVCLK1;
    SET_HVCLK1;
  }
  SET_HVLTC1;
}

uint16_t HVDCgetVoltageSetPoint(hvdc_t* hvdc_ptr, time_t* time_ptr)
{
  uint16_t Vsp = hvdc_ptr->setPointDefault;
  /* Use higher voltage first minute to heaten tubes, or when setting time */
  if (time_ptr->min < 1 || time_ptr->setTimeMode)
    Vsp = hvdc_ptr->setPointFire;

  return (Vsp);
}

void HVDCinit(hvdc_t* hvdc_ptr, float spDefault, float spFire)
{
  hvdc_ptr->setPointDefault = ((uint16_t) (spDefault/((float) HVDC_VOLT_PER_LSB)));
  hvdc_ptr->setPointFire = ((uint16_t) (spFire/((float) HVDC_VOLT_PER_LSB)));
}