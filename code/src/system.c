#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include "system.h"
#include "user.h"

uint8_t Tmr2PreScaler = 1;
uint32_t PwmFreq = 30000;
uint8_t pwmPR2;

void ConfigureOscillator(void)
{
  OSCCON = 0b01110000;            // Internal clock at 8 MHz
  while(!(OSCCON & 0b00000100));  // Wait until stable clock
}

void InitPeriph(void)
{

  ConfigureOscillator();

  TRISA = 0xFF;               // RA4: 32 kHz (in)
  TRISA &= ~(HVDTA1_PIN | HVCLK1_PIN | DEBUG_PIN);
  TRISB = 0b11110111;         // RB1: BTN1 (in)
                              // RB2: BTN2 (in)
                              // RB3: PWM (out)
                              // RB4: FB (in)
                              // RB5: BTN3 (in)
  TRISB &= ~(HVLTC1_PIN);

  /* Tmr0 8 bit for delay function */
  T0CON = 0b11100100;         // External clock=32768Hz and prescaler=1:32
                              // 8 bit overflow => 4 Hz
  INTCON = 0b10100000;        // Interrupt enabled

  /* PWM on RB3*/
  CCP1CON = 0b00001100;       // P1 (on RB3) only PWM, active high
  T2CON &= 0b11111100;        // Tmr2 prescaler 1:1
  pwmPR2 = PwmSetFreq(PwmFreq);
  CCPR1L = 0x00;
  PIR1 &= 0b11111101;         // Clear Tmr2 interrupt flag
  T2CON |= 0b00000100;        // Enable Tmr2

  /* ADC on RB4 (AN6) */
  ADCON0 = 0b00000000;
  ADCON1 = 0b10111111;        // AN6 analog
  ADCON2 = 0b00111101;        // TAD = 16Tosc, Tacq = 100us/16Tosc = 50>20 TAD
  ADCON0 |= 0b00000001;       // Turn on ADC
}

uint16_t ADGetRes(uint8_t ch)
{
  uint16_t timeOut = AD_CONVERSION_TIMEOUT;

  ADCON0 &= 0b11100011;                                 // Chose input channel
  ADCON0 |= ((ch & 0x07) << 2);
  ADCON0 |= 0b00000010;                                 // Set GO/!DONE bit

  while((ADCON0 & 0b00000010) && timeOut--);            // Wait until !DONE

  return ( (uint16_t)((ADRESH<<2) | (ADRESL & 0x03)) ); // Get result
}

void PwmSetDuty(float d)
{
  PwmDuty_10bit((uint16_t) (((float)((PR2+1)*4)) * d) );
}

uint8_t PwmSetFreq(uint32_t f)
{
  /* PWM freq = Fosc/((PR2+1)*4*Tmr2PreScaler)
              = Fcy/((PR2+1)*Tmr2PreScaler) */

  PR2 = (uint8_t) ((FCY / (f*Tmr2PreScaler)) - 1); // 132
  return PR2;
}

void PwmDuty_10bit(uint16_t d)
{
  //d &= 50; // Hard limit
  CCP1CON &= 0b11001111;
  CCP1CON |= ((uint8_t) (d & 0x03)) << 4;
  CCPR1L = (uint8_t) ((d>>2) & 0xFF);
  uint8_t MSb = CCPR1L;
  uint8_t LSb = (CCP1CON & 0b00110000) >> 4;
}