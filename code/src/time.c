#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include "time.h"
#include "user.h"

extern uint8_t btnSetReleased;

uint8_t ShiftMap_A[T1NNBRS+1] =  {SHIFTMAP_A0,
                                  SHIFTMAP_A1,
                                  SHIFTMAP_A2,
                                  SHIFTMAP_A3,
                                  SHIFTMAP_A4,
                                  SHIFTMAP_A5,
                                  SHIFTMAP_A6,
                                  SHIFTMAP_A7,
                                  SHIFTMAP_A8,
                                  SHIFTMAP_A9,
                                  SHIFTMAP_NAN};
uint8_t ShiftMap_B[T1NNBRS+1] =  {SHIFTMAP_B0,
                                  SHIFTMAP_B1,
                                  SHIFTMAP_B2,
                                  SHIFTMAP_B3,
                                  SHIFTMAP_B4,
                                  SHIFTMAP_B5,
                                  SHIFTMAP_B6,
                                  SHIFTMAP_B7,
                                  SHIFTMAP_B8,
                                  SHIFTMAP_B9,
                                  SHIFTMAP_NAN};
uint8_t ShiftMap_C[T1NNBRS+1] =  {SHIFTMAP_C0,
                                  SHIFTMAP_C1,
                                  SHIFTMAP_C2,
                                  SHIFTMAP_C3,
                                  SHIFTMAP_C4,
                                  SHIFTMAP_C5,
                                  SHIFTMAP_C6,
                                  SHIFTMAP_C7,
                                  SHIFTMAP_C8,
                                  SHIFTMAP_C9,
                                  SHIFTMAP_NAN};
uint8_t ShiftMap_D[T1NNBRS+1] =  {SHIFTMAP_D0,
                                  SHIFTMAP_D1,
                                  SHIFTMAP_D2,
                                  SHIFTMAP_D3,
                                  SHIFTMAP_D4,
                                  SHIFTMAP_D5,
                                  SHIFTMAP_D6,
                                  SHIFTMAP_D7,
                                  SHIFTMAP_D8,
                                  SHIFTMAP_D9,
                                  SHIFTMAP_NAN};

time_t time = {0,0,0,0,0,0,TIMESETMODEOFF,0,0,0};

void timeIncDecMin(time_t* time_ptr, uint8_t inc)
{
  time_ptr->sec = 0;

  if (inc && time_ptr->min++ == 59) {
    time_ptr->min = 0;
  } else if (!inc && time_ptr->min-- == 0) {
    time_ptr->min = 59;
  }
}

void timeIncDecHour(time_t* time_ptr, uint8_t inc)
{
  time_ptr->sec = 0;

  if (inc && time_ptr->hour++ == 23) {
    time_ptr->hour = 0;
  } else if (!inc && time_ptr->hour-- == 0) {
    time_ptr->hour = 23;
  }
}

void timeTick(time_t* time_ptr)
{

  if (time_ptr->sec++ == 59) {
    time_ptr->sec = 0;
    if (time_ptr->min++ == 59) {
      time_ptr->min = 0;
      if (time_ptr->hour++ == 23) {
        time_ptr->hour = 0;
      }
    }
  }
}

void timeOutput(time_t* time_ptr)
{
  uint32_t out_pattern;
  uint8_t n;
  uint8_t h_10, h_1, m_10, m_1;

  h_10 = time_ptr->hour/10;
  h_1 = time_ptr->hour - 10*h_10;
  m_10 = time_ptr->min/10;
  m_1 = time_ptr->min - 10*m_10;

  if (time.setTimeMode == TIMESETMODEMIN && time.toggle2Hz) {
    m_10 = T1NNBRS;
    m_1 = T1NNBRS;
  } else if (time.setTimeMode == TIMESETMODEHOUR && time.toggle2Hz) {
    h_10 = T1NNBRS;
    h_1 = T1NNBRS;
  }

  out_pattern = (((uint32_t)(1)) << ShiftMap_C[m_10]) | (((uint32_t)(1)) << ShiftMap_D[m_1]);
  out_pattern = ~(0xFFFFF & out_pattern);

  CLEAR_HVLTC1;

  for (n=0; n<HVNOUTTOT; n++) {
    CLEAR_HVCLK1;
    if ((out_pattern>>n) & 0x1)
      SET_HVDTA1;
    else
      CLEAR_HVDTA1;
    SET_HVCLK1;
  }

  out_pattern = (((uint32_t)(1)) << ShiftMap_A[h_10]) | (((uint32_t)(1)) << ShiftMap_B[h_1]);
  out_pattern = ~(0xFFFFF & out_pattern);

  for (n=0; n<HVNOUTTOT; n++) {
    CLEAR_HVCLK1;
    if ((out_pattern>>n) & 0x1)
      SET_HVDTA1;
    else
      CLEAR_HVDTA1;
    SET_HVCLK1;
  }

  SET_HVLTC1;
}

void timeSet(time_t* time_ptr)
{
  if (btnUpPressed()) {
    time_ptr->setCntr = 0;
    if (time_ptr->setTimeMode == TIMESETMODEMIN) {
      timeIncDecMin(time_ptr, 1);
    } else {
      timeIncDecHour(time_ptr, 1);
    }
  } else if (btnDownPressed()) {
    time_ptr->setCntr = 0;
    if (time_ptr->setTimeMode == TIMESETMODEMIN) {
      timeIncDecMin(time_ptr, 0);
    } else {
      timeIncDecHour(time_ptr, 0);
    }
  } else if (btnSetPressed()) {
    time_ptr->setCntr = 0;
    btnSetReleased = 0;
    if (time_ptr->setTimeMode == TIMESETMODEMIN)
      time_ptr->setTimeMode = TIMESETMODEHOUR;
    else
      time_ptr->setTimeMode = TIMESETMODEMIN;
  } else if (time_ptr->setCntr++ > 20) {
    btnSetReleased = 0;
    time_ptr->setTimeMode = TIMESETMODEOFF;
    time_ptr->setCntr = 0;
  } 
}