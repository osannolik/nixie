#include "time.h"

#define DEBUG_PIN               (0b00000001)    // Rx0
#define DEBUG_PORT              (PORTA)
#define SET_DEBUG               (DEBUG_PORT |= DEBUG_PIN)
#define CLEAR_DEBUG             (DEBUG_PORT &= (~DEBUG_PIN))

#define HVCLK1_PORT             (PORTA)
#define HVDTA1_PORT             (PORTA)
#define HVLTC1_PORT             (PORTB)
#define HVCLK1_PIN              (0b00001000)    // Rx3
#define HVDTA1_PIN              (0b00000100)    // Rx2
#define HVLTC1_PIN              (0b00000001)    // Rx0
#define SET_HVCLK1              (HVCLK1_PORT |= HVCLK1_PIN)
#define SET_HVDTA1              (HVDTA1_PORT |= HVDTA1_PIN)
#define SET_HVLTC1              (HVLTC1_PORT |= HVLTC1_PIN)
#define CLEAR_HVCLK1            (HVCLK1_PORT &= (~HVCLK1_PIN))
#define CLEAR_HVDTA1            (HVDTA1_PORT &= (~HVDTA1_PIN))
#define CLEAR_HVLTC1            (HVLTC1_PORT &= (~HVLTC1_PIN))

#define BTN1_PORT               (PORTB)
#define BTN2_PORT               (PORTB)
#define BTN3_PORT               (PORTB)
#define BTN1_PIN                (0b00000010)
#define BTN2_PIN                (0b00000100)
#define BTN3_PIN                (0b00100000)

#define HVDC_SETPOINT_DEFAULT   (135.0)
#define HVDC_SETPOINT_FIRE      (160.0)
#define HVDC_CTRL_GAIN_P(X)     ((X)/2)
#define HVDC_CTRL_GAIN_I(X)     ((X)/8)

#define PWM_DUTY_BITS           (10)
#define PWM_MAX_DUTY            ((((uint16_t)1)<<(PWM_DUTY_BITS))-1)
#define HVDC_VOLT_DIVIDER_RATIO (69.3)
#define HVDC_VOLT_PER_LSB       (HVDC_VOLT_DIVIDER_RATIO*5.0/PWM_MAX_DUTY)

typedef struct {
    uint16_t setPointDefault;
    uint16_t setPointFire;
} hvdc_t;

uint8_t btn1Pressed(void);
uint8_t btn2Pressed(void);
uint8_t btn3Pressed(void);
uint8_t btnSetPressed(void);
uint8_t btnDownPressed(void);
uint8_t btnUpPressed(void);
void HvSetAll(void);
uint16_t HVDCgetVoltageSetPoint(hvdc_t* hvdc_ptr, time_t* time_ptr);
void HVDCinit(hvdc_t* hvdc_ptr, float spDefault, float spFire);
