/* Microcontroller MIPs (FCY) */
#define SYS_FREQ                8000000L
#define FCY                     SYS_FREQ/4

#define AD_CONVERSION_TIMEOUT   (10000)

void ConfigureOscillator(void);
void InitPeriph(void);
void PwmDuty_10bit(uint16_t d);
uint8_t PwmSetFreq(uint32_t f);
void PwmSetDuty(float d);
uint16_t ADGetRes(uint8_t ch);