#define SHIFTMAP_NAN            (20) // any value > 19

#define SHIFTMAP_A0             (0)
#define SHIFTMAP_A1             (19)
#define SHIFTMAP_A2             (18)
#define SHIFTMAP_A3             (17)
#define SHIFTMAP_A4             (16)
#define SHIFTMAP_A5             (15)
#define SHIFTMAP_A6             (4)
#define SHIFTMAP_A7             (3)
#define SHIFTMAP_A8             (2)
#define SHIFTMAP_A9             (1)

#define SHIFTMAP_B0             (10)
#define SHIFTMAP_B1             (9)
#define SHIFTMAP_B2             (8)
#define SHIFTMAP_B3             (7)
#define SHIFTMAP_B4             (6)
#define SHIFTMAP_B5             (5)
#define SHIFTMAP_B6             (14)
#define SHIFTMAP_B7             (13)
#define SHIFTMAP_B8             (12)
#define SHIFTMAP_B9             (11)

#define SHIFTMAP_C0             (0)
#define SHIFTMAP_C1             (19)
#define SHIFTMAP_C2             (18)
#define SHIFTMAP_C3             (17)
#define SHIFTMAP_C4             (16)
#define SHIFTMAP_C5             (15)
#define SHIFTMAP_C6             (4)
#define SHIFTMAP_C7             (3)
#define SHIFTMAP_C8             (2)
#define SHIFTMAP_C9             (1)

#define SHIFTMAP_D0             (10)
#define SHIFTMAP_D1             (9)
#define SHIFTMAP_D2             (8)
#define SHIFTMAP_D3             (7)
#define SHIFTMAP_D4             (6)
#define SHIFTMAP_D5             (5)
#define SHIFTMAP_D6             (14)
#define SHIFTMAP_D7             (13)
#define SHIFTMAP_D8             (12)
#define SHIFTMAP_D9             (11)

#define T1NNBRS                 (10)
#define HVNOUTTOT               (20)

#define TIMESETMODEOFF          (0)
#define TIMESETMODEMIN          (1)
#define TIMESETMODEHOUR          (2)

typedef struct {
  uint8_t sec;
  uint8_t min;
  uint8_t hour;
  uint8_t doTick;
  uint8_t doSet;
  uint8_t setCntr;
  uint8_t setTimeMode;
  uint8_t setSecPressed;
  uint8_t toggle2Hz;
  uint8_t toggle1Hz;
} time_t;

void timeOutput(time_t* time_ptr);
void timeTick(time_t* time_ptr);
void timeSet(time_t* time_ptr);
void timeIncDecMin(time_t* time_ptr, uint8_t inc);
void timeIncDecHour(time_t* time_ptr, uint8_t inc);