# A simple python script used to copy files from repo to MPLABX project dir prior to build. 
# You can setup MPLABX to run this script automatically in project settings -> Building:
# 
# * Place this script in project root
# * Check "Execute this line before build"-box in project settings -> Building
# * Add line ./copy_code.py /the/path/to/repo/code
#
# This way you can more easily edit the source code in the repo and only use MPLABX for 
# compiling and debugging. Note that you need to ensure that MPLABX knows that source and 
# headers can be found in <projectroot>/src and <projectroot>/inc. 

#!/usr/bin/python
import sys
import os
from os import listdir
from os.path import isfile, join
import shutil

if os.path.exists(sys.argv[1]):
  srcpath = sys.argv[1]

  destpath = os.getcwd()

  foldersToCpy = ["src","inc"]

  for folder in foldersToCpy:
    path = join(srcpath,folder)
    dest = join(destpath,folder)
    # List files in path
    onlyfiles = [ f for f in listdir(path) if (isfile(join(path,f)) and f != ".DS_Store") ]

    if not os.path.exists(dest):
      os.makedirs(dest)
    print "Copied:"
    for f in onlyfiles:
      src = join(path,f)
      shutil.copy(src, dest)
      print "  "+f
    print "to: "+dest
  
else:
  print "Sorry, source path"
  print "  "+sys.argv[1]
  print "does not exist"