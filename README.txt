Nixie tube clock project

The tubes used are the IN-14 which are driven by the high voltage display driver HV5812. 
Time is kept by a PIC18F1320 using the DS32KHZ as an oscillator. 
The PIC also controls the step-up converter responsible for converting 12 V input to the ~150 V required
to fire the nixie tubes. 

Code has been confirmed to compile with xc8 v1.33 using MPLAB X v1.95. 

PCB design files in kicad format can be found in the kicad folder. 
They have been produced using kicad 2014-12-26 BZR 5338 built for OSX 10.9.5. 

Author: osannolik@godtycklig.se
